<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-gis?lang_cible=ja
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// G
	'gis_description' => 'このプラグインで、地理的に配置されたポイントを作成します。ポイントを、SPIPオブジェクトに添付すると、ページの地図上に表示することになります。
リーフレットライブラリのおかげで、さまざまなサプライヤーの地図の背景を使用することができます。',
	'gis_slogan' => '地理情報システム'
);
